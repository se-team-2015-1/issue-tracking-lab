program lab3;

uses output, processing;

var
    matrix: SquareMatrix;

begin
    FillMatrix(matrix);
    PrintMatrix(matrix);
    WriteLn;
    PrintNumber(GetNumberMainDiagonalNegative(matrix));
    SetToZeroAboveSecondaryDiagonalPositive(matrix);
    WriteLn;
    PrintMatrix(matrix);
    ReadLn;
end.

