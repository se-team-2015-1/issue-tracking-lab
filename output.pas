unit output;

interface

const
    MatrixSize = 9;

type
    SquareMatrix = array [1..MatrixSize, 1..MatrixSize] of Integer;

procedure PrintMatrix(var matrix: SquareMatrix);
procedure PrintNumber(number: Integer);

implementation

procedure PrintMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            Write (matrix[i, j]: 4);
        end;
        WriteLn;
        WriteLn;
    end;
end;

procedure PrintNumber(number: Integer);
begin
    Write('Main diagonal negative elemets number = ');
    WriteLn(number);
    WriteLn;
end;

end.

