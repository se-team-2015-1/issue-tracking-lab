unit processing;

interface

const
    MatrixSize = 9;
    HighBound = 13;
    LowBound = -15;
    NumbersQuantity = HighBound - LowBound + 1;

type
    SquareMatrix = array [1..MatrixSize, 1..MatrixSize] of Integer;

function GetNumber: Integer;
procedure FillMatrix(var matrix: SquareMatrix);
function GetNumberMainDiagonalNegative(var matrix: SquareMatrix):Integer;
procedure SetToZeroAboveSecondaryDiagonalPositive(var matrix: SquareMatrix);

implementation

function GetNumber: Integer;
begin
    GetNumber := Random(NumbersQuantity) + LowBound;
end;

procedure FillMatrix(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize do
    begin
        for j := 1 to MatrixSize do
        begin
            matrix[i, j] := GetNumber;
        end;
    end;
end;

function GetNumberMainDiagonalNegative(var matrix: SquareMatrix):Integer;
var
    i, j, number: Integer;
begin
    number := 0;
    for i := 1 to MatrixSize do
    begin
        if matrix[i, i] < 0 then
        begin
            number := number + 1;
        end;
    end;
    GetNumberMainDiagonalNegative := number;
end;

procedure SetToZeroAboveSecondaryDiagonalPositive(var matrix: SquareMatrix);
var
    i, j: Integer;
begin
    for i := 1 to MatrixSize - 1 do
    begin
        for j := 1 to MatrixSize - i do
        begin
            if matrix[i, j] > 0 then
            begin
                matrix[i, j] := 0;
            end;
        end;
    end;
end;

end.

